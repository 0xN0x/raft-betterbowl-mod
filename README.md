# BetterBowl
*([view on RaftModding](https://www.raftmodding.com/mods/betterbowl))*

BetterBowl just gives you an empty bowl when you eat soup or food on a plate, but sometimes the bowl breaks.

### Why?
In vanilla Raft, you have to use a bowl to craft dishes like "Fishstew" or "Drumstick with jam". When you eat the food, the bowl just dissapears. Makes no sense, right? This is where BetterBowl steps in!

### Installing the mod
Download the mod file by clicking the `Download` button on the right and install it as shown in our [mod installation guide](https://api.raftmodding.com/tutorials/how-to-install-a-mod).

### How to use
Just eat some soup and you will get your bowl back in most cases. Your bowl will usually break every **3rd** to **5th** time you eat soup. If you want to change how often the bowl breaks, have a look at the `breakInt` command below.

### Changing the break interval
**Command:** `breakInt <minUses> <maxUses>`:
* `<minUses>` should be replaced with the minimal amount of bowl uses before it breaks.
* `<maxUses>` should be replaced with the maximal amount of bowl uses before it breaks. 

**Example:** `breakInt 4 6` will make every 4th to 6th bowl break.

### Credits
* original idea: [TeigRolle](https://www.raftmodding.com/user/TeigRolle)
* code: [traxam](https://trax.am)
* bowl icon: [Freepik](https://www.flaticon.com/authors/freepik) from [www.flaticon.com](https://www.flaticon.com/)